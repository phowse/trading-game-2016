﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Awesome_Trading_Game
{
    class Town
    {
        Random rnd = new Random();

        // Return the name of the Town
        public string getName()
        {
            return name;
        }

        // Gets a price for Six Shooters
        public int getSixShooterPrice()
        {
            // Generates a price using the following formula:
            // base price + random number between low and high
            return this.six_shooter_base + rnd.Next(this.six_shooter_low, this.six_shooter_high);
        }

        // Gets a price for shovels
        public int getShovelsPrice()
        {
            return this.shovels_base + rnd.Next(this.shovels_low, this.shovels_high);
        }

        // Method to generate a list of all prices to make our lives easier
        public int[] getListOfPrices(){

            int[] arrayPrices = new int[6];

            arrayPrices[0] = this.getSixShooterPrice();
            arrayPrices[1] = this.getShovelsPrice();

            return arrayPrices;

        }

        // Declare the object's variables and how we can set them and use them
        public string name { get; set; }
        public int six_shooter_base { get; set; }
        public int six_shooter_low { get; set; }
        public int six_shooter_high { get; set; }
        public int shovels_base { get; set; }
        public int shovels_low { get; set; }
        public int shovels_high { get; set; }

    }
}
